![alt text](https://i.imgur.com/nINOiBE.png "Covid19Dashboard")
# Covid19Dashboard  
The Covid19Dashboard is an interactive web platform built with Vue.js and Vuetify to display statistics and data about the Covid19 virus.  
Showing data from John Hopkins University CSSE. See mathdroid/covid19.  

## Demo
Unter covid19.haugeneder.dev findest du eine Demo des Projekts.

## Starte das Projekt lokal 
```
npm i
vue-cli-service serve
```
## Tested e2e with Cypress
```
vue-cli-service test:e2e
```
## Screenshots  
![alt text](https://i.imgur.com/a7FkkL7.png "Covid19Dashboard")

## Dependencies
```
"core-js": "^3.6.5",
"vue": "^2.6.11",
"vue-router": "^3.2.0",
"vuetify": "^2.2.11"
"@vue/cli-plugin-babel": "~4.4.0",
"@vue/cli-plugin-e2e-cypress": "~4.4.0",
"@vue/cli-plugin-eslint": "~4.4.0",
"@vue/cli-plugin-router": "^4.4.1",
"@vue/cli-service": "~4.4.0",
"@vue/eslint-config-prettier": "^6.0.0",
"babel-eslint": "^10.1.0",
"eslint": "^6.7.2",
"eslint-plugin-prettier": "^3.1.3",
"eslint-plugin-vue": "^6.2.2",
"node-sass": "^4.12.0",
"prettier": "^1.19.1",
"sass": "^1.19.0",
"sass-loader": "^8.0.2",
"vue-cli-plugin-vuetify": "^2.0.5",
"vue-template-compiler": "^2.6.11",
"vuetify-loader": "^1.3.0"
```

## License

[ISC License ](https://choosealicense.com/licenses/isc/)

**Copyright (c) 2019, Julian Haugeneder** 

Permission to use, copy, modify, and/or distribute this software for any  
purpose with or without fee is hereby granted, provided that the above  
copyright notice and this permission notice appear in all copies.  

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF  
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR  
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN  
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF  
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
